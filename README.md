[1]: https://bitbucket.org/rteyssie/ramses/
[2]: https://bitbucket.org/ableuler/mini-ramses/fork
[3]: https://bitbucket.org/ableuler/mini-ramses/pull-requests
[4]: https://bitbucket.org/ableuler/mini-ramses/wiki

## mini-ramses ##

The mini-ramses repository is a fork of the [main RAMSES repository][1]. It contains a stripped-down version of the main code base created in order to facilitate the development of major updates of RAMSES' core routines. 

You *could* download the code by cloning the git repository using 
```
$ git clone https://bitbucket.org/ableuler/mini-ramses.git
```
However, as you probably want to mess around with the code, we suggest you rather [fork mini-ramses][2]. To get changes back into this repository, you simply create a [pull request][3].

For more information, check out the [mini-ramses wiki][4].