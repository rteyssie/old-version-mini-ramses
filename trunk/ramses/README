mini-ramses version 1.0

mini-ramses is a development platform for the RAMSES code. It is a stripped-down 
version of the main code base created in order to facilitate the development of 
major updates of RAMSES' core routines. 

mini-ramses is available at
https://bitbucket.org/ableuler/mini-ramses

================================================================================
About RAMSES:

Copyright Romain Teyssier and CEA from 1997 to 2007

Copyright Romain Teyssier and the University of Zurich from 2008 to 2013

romain.teyssier@gmail.com

This software is  an open source computer program whose  purpose is to
perform  simulations of  self-gravitating  fluids  with Adaptive  Mesh
Refinement on massively parallel computers. It is based on the Fortran
90 language and the MPI communication library.

When using the RAMSES code, please cite the following paper for proper
credit:

Teyssier, R., "Cosmological hydrodynamics with adaptive mesh refinement. 
A new high resolution code called RAMSES", 2002, A&A, 385, 337

The code is available for download and documentation in
https://bitbucket.org/rteyssie/ramses
================================================================================

You can quickly test your installation by executing:
$ cd bin
$ make
$ cd ..
$ bin/ramses1d namelist/tube1d.nml

You can run the adiabatic halo benchmark test with MPI on 8 processors:
(We are assuming that you are using the gfortran compiler and have an MPI 
library installed)

$ wget http://www.ics.uzh.ch/~ableuler/mini-ramses_ICs/adiabatic_halo/ICs.tar
$ tar -xvf ICs.tar
$ cd bin
$ make -f ../namelist/mini-ramses_benchmarking/Makefile.adiabatic_halo
$ cd ..
$ mpirun -np 8 bin/ramses3d namelist/mini-ramses_benchmarking/adiabatic_halo.nml

Enjoy !
