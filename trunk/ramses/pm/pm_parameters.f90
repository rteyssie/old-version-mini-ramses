module pm_parameters
  use amr_parameters, ONLY: dp
  integer::npartmax=0               ! Maximum number of particles
  integer::npart=0                  ! Actual number of particles

end module pm_parameters
