
Representative RAMSES tescase that can be used to experiment with mini-ramses. 

Ingredients:
- self gravity
- adiabatic hydro
- dark matter particles

- An example Makefile (assuming gfortran compiler) for an MPI run is provided in this 
  directory.

- The initial condition files can be downloaded as a tarball from:
  http://www.ics.uzh.ch/~ableuler/mini-ramses_ICs/adiabatic_halo/ICs.tar
